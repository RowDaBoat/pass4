# Pass4

Pass4 is a simple password generator for websites. It uses a private generated master key, and a configuration file, so portability among any *NIX device is easy. If a generated password is leaked, a new one can be issued.

Pass4 generates passwords like so: 

```
hash([master_key]-[pass_id]@[site])
```

whereas `pass_id` is an incremental id each time a new password is requested, `site` is the target site, and `master_key` is a user's generated secret.

## Installation

Just clone the repo and run

    $ sh install.sh

## Usage

Just run:

    $ pass4 somewebsite.net

and a password generated for that site will be copied in the clipboard.

Use:

    $ pass4 --append=_A1 somewebsite.net

to meet any specific requirements.

Use:

    $ pass4 --new somewebsite.net

to generate a new password for `somewebsite.net` if the previous one was leaked.

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/RowDaBoat/pass4.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
