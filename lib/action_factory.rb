require_relative 'domain/actions/copy_pass.rb'
require_relative 'domain/actions/new_pass.rb'
require_relative 'domain/actions/set_key.rb'
require_relative 'domain/actions/show_words.rb'
require_relative 'domain/actions/show_sites.rb'
require_relative 'domain/actions/remove_site.rb'
require_relative 'domain/actions/shorten.rb'
require_relative 'domain/actions/append.rb'
require_relative 'domain/actions/reset_length.rb'
require_relative 'domain/actions/new_key.rb'
require_relative 'infrastructure/configuration.rb'
require_relative 'infrastructure/key.rb'
require_relative 'infrastructure/passes.rb'

def configuration
  @configuration ||= Configuration.new
end

def passes
  @passes ||= Passes.new(configuration)
end

def key
  @key ||= Key.new(configuration)
end

def show_sites
  ShowSites.new(passes)
end

def new_pass
  NewPass.new(passes, key)
end

def show_words
  ShowWords.new(key)
end

def set_key
  SetKey.new(passes, key)
end

def copy_pass
  CopyPass.new(passes, key)  
end

def remove_site
  RemoveSite.new(passes)
end

def shorten
  Shorten.new(passes)
end

def append
  Append.new(passes)
end

def reset_length
  ResetLength.new(passes)
end

def new_key
  NewKey.new(key, passes)
end
