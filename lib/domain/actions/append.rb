require_relative '../fail.rb'

class Append
  def initialize passes
    @passes = passes
  end

  def do(site, to_append)
    appendage = validate_appendage(to_append)
    pass = @passes.get_for(site) || PassProperties.new
    @passes.save(pass.append(to_append), site)
  end

  def validate_appendage to_append
    to_append.nil? || to_append.empty? ? '' : to_append
  end
end
