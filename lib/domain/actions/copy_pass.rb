require_relative '../hash.rb'
require_relative '../clipboard.rb'

class CopyPass
  def initialize(passes, key)
    @passes = passes
    @key = key
  end

  def for site
    fail_with "specify a site to copy a password for" if site.nil? || site.empty?

    save_new_pass_for site unless pass_exists_for site

    copy_to_clipboard @passes
      .get_for(site)
      .hash_password(@key.get, site)
  end

  def save_new_pass_for site
    @passes.save(PassProperties.new, site)
  end

  def pass_exists_for site
    !@passes.get_for(site).nil?
  end
end
