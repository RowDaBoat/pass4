class NewKey
  def initialize(key, passes)
    @key = key
    @passes = passes
  end

  def do
    @key.save(@key.new_key)
  end
end
