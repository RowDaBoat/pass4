require_relative '../clipboard.rb'
require_relative '../pass_properties.rb'

class NewPass
  def initialize(passes, key)
    @passes = passes
    @key = key
  end

  def for site
    pass_properties = @passes.get_for(site)

    new_pass_properties = pass_properties.nil? ?
      PassProperties.new : pass_properties.next_pass

    @passes.save(new_pass_properties, site)
  
    copy_to_clipboard new_pass_properties.hash_password(@key.get, site)
  end
end
