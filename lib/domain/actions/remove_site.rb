class RemoveSite
  def initialize(passes)
    @passes = passes
  end

  def do site
    @passes.remove(site)
  end
end
