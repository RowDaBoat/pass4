class ResetLength
  def initialize passes
    @passes = passes
  end

  def do(site)
    pass = @passes.get_for(site) || PassProperties.new
    @passes.save(pass.reset_length, site)
  end
end
