require_relative '../fail.rb'
require_relative '../words.rb'

KEY_WORDS_COUNT = 16

class SetKey
  def initialize(passes, key)
    @passes = passes
    @key = key
  end

  def from words
    fail_with not_enough_words(words.length) if words.length < KEY_WORDS_COUNT
    warn too_many_words(words) if words.length > KEY_WORDS_COUNT

    result = hex_values_from words

    fail_with invalid_words(result[:invalid_words]) unless result[:invalid_words].empty?

    @key.save create_key(result[:valid_values])
    @passes.each { |site| @passes.save(PassProperties.new, site) }
  end

  def create_key hex_values
    hex_values.join.downcase
  end

  def not_enough_words count 
    "not enough words to form key. 16 words are required, only #{count} were passed."
  end

  def invalid_words words
    "these words are not valid:\n#{show_words(words)}"
  end

  def too_many_words words
    "these words are excess and will be ignored:\n#{show_words(words.drop(KEY_WORDS_COUNT))}"
  end

  def show_words words
    "[#{words.join(' ')}]"
  end
end
