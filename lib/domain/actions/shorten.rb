class Shorten
  def initialize passes
    @passes = passes
  end

  def do(site, length_argument)
    length = validate_length(length_argument.to_i)
    pass = @passes.get_for(site) || PassProperties.new
    @passes.save(pass.with_length(length), site)
  end

  def validate_length length
    length = 8 if length.nil? || length.zero?
    length
  end
end
