class ShowSites
  def initialize(passes)
    @passes = passes
  end

  def do(filter_by)
    filter_by = "" if filter_by.nil?
    @passes
      .filter {|site| site.include? filter_by }
      .each { |site| puts site }
  end
end
