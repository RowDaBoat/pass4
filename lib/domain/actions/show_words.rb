require_relative '../words.rb'

class ShowWords
  def initialize key
    @key = key
  end

  def do
    puts to_words @key.get
  end
end
