require 'clipboard'

def copy_to_clipboard value
  Clipboard.copy value
end
