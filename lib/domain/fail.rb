def fail_with message
  STDERR.puts "Error, #{message}\n"
  exit 1
end

def warn message
  STDERR.puts "Warning, #{message}\n"
end
