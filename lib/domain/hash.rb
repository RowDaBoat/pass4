require 'digest'

def hash_key_with_site(key, pass_id, at)
  remove_hyphens(sha1_hash("#{key}-#{pass_id}@#{at}"))
end

def remove_hyphens hashed_key
  hashed_key.gsub('-', '')
end

def sha1_hash value
  Digest::SHA1.hexdigest value
end  
