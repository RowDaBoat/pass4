require_relative 'hash.rb'

class PassProperties
  BIG_PASSWORD = 40

  def self.from hash
    PassProperties.new(hash[:id], hash[:length], hash[:appendage])
  end

  def initialize(id = 0, length = nil, appendage = nil)
    @id = id
    @length = length
    @appendage = appendage || ''
  end

  def next_pass
    PassProperties.new(@id + 1, @length, @appendage)
  end

  def with_length length
    PassProperties.new(@id, length, @appendage)
  end

  def append to_append
    PassProperties.new(@id, @length, to_append)
  end

  def reset_length
    PassProperties.new(@id, nil, @appendage)
  end

  def hash_password(key, site)
    shorten(hash_key_with_site(key, @id, site)) + @appendage
  end

  def shorten(key)
    key[0, @length || BIG_PASSWORD]
  end

  def id
    @id
  end

  def length
    @length
  end

  def to_hash
    @length.nil? ?
      { id: @id, appendage: @appendage } :
      { id: @id, length: @length, appendage: @appendage }
  end
end
