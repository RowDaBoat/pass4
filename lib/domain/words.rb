require 'pgp-word-list'

def to_words key
  key.to_pgp_words
end

def hex_values_from words
  hex_values = take_hex_values words
  {
    :valid_values => hex_values,
    :invalid_words => filter_invalid_words(words, hex_values)
  }
end

def take_hex_values words
  words.take(16).to_pgp_hex
end

def filter_invalid_words(words, hex_values)
  hex_values.map
    .with_index { |hex_value, i| i if hex_value.nil? }
    .compact
    .map { |i| words[i] }
end
