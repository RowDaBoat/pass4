def usage
  "Usage: #{$0} [target site]\n" +
  "Copies a generated password for [target site] into the clipboard.\n"
end

def discard_first_line help
  help.lines[2..-1].join
end

def help opts
  puts "#{usage}\n#{discard_first_line opts.help}"
end
