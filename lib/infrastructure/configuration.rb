class Configuration
  def initialize
    Dir.mkdir path unless Dir.exists?(path)
  end

  def path
    File.expand_path '~/.pass4'
  end
end
