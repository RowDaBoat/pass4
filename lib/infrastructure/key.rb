require 'securerandom'
require 'fileutils'

class Key
  def initialize(configuration)
    @configuration = configuration

    save(new_key) unless it_exists?
    @value = load
  end

  def get
    @value
  end
  
  def save key
    save(key)
    @value = key
  end

  def file
    'key'
  end

  def new_key
    SecureRandom.uuid.gsub('-', '')
  end

  def path
    "#{@configuration.path}/#{file}"
  end

  def it_exists?
    File.file?(path)
  end

  def save key  
    File.open(path, "w", 0600) { |file| file.write key }
  end

  def load
    File.read(path)
  end
end
