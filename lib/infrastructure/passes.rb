require 'json'
require_relative '../domain/pass_properties.rb'

class Passes
  include Enumerable

  def initialize(configuration)
    @configuration = configuration

    create_empty_passwords_file unless passwords_file_exists
    @passwords = JSON.parse(open_passwords_file)
  end

  def save(pass_properties, site)
    @passwords[site] = pass_properties.to_hash
    write_to_passwords_file
  end

  def remove site
    @passwords.delete site
    write_to_passwords_file
  end

  def exists site
    @passwords.key?(site)
  end

  def get_for(site)
    pass_properties = @passwords[site]
    pass_properties.nil? ? nil : PassProperties.from(pass_properties.transform_keys(&:to_sym))
  end

  def each
    @passwords.keys.each { |site| yield site }
  end

  def all_sites
    @passwords.keys.each { |site| yield site }
  end

  def passwords_file
    "#{@configuration.path}/passwords.json"
  end

  def empty_passwords
    {}
  end

  def passwords_file_exists
    File.exists?(passwords_file)
  end

  def create_empty_passwords_file
    File.write(passwords_file, empty_passwords.to_json)
  end

  def open_passwords_file
    File.read(passwords_file)
  end

  def write_to_passwords_file
    File.write(passwords_file, @passwords.to_json)
  end
end
