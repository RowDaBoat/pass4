#!/usr/bin/env ruby

require 'optparse'

require_relative 'help.rb'
require_relative 'action_factory.rb'

require 'json'

module Pass4
  class CLI
    ARGV << '-h' if ARGV.empty?
    site = ARGV.last

    OptionParser.new do |opts|
      opts.separator ""
      opts.separator "Options:"

      help = {
        :short => "Gives a short password, ideal for devices that can't run pass4, this will shorten your current password",
        :append => "Appends a string at the end of the password, use this to meet conditions of sites with complicated rules",
        :long => "Gives a long password, use it to reset the current password's length",
        :sites => "Shows sites used before",
        :help => "Prints this help",
        :new => "Discard the previous key for this site, then generate a new one and copy it",
        :show_key_words => "Show the key words that form your master key",
        :set_key_from_key_words => "Sets a new master key from the key 16 words given as arguments",
        :new_master_key => "Erase the master key and create a new one, all sites passes are reset",
        :remove => "Remove a site from the passwords list"
      }
      
      opts.on("--short[=LENGTH]", help[:short]) {
        |length| shorten.do(site, length)
      }

      opts.on("--append=[STRING]", help[:append]) do |to_append| 
        append.do(site, to_append)
      end

      opts.on("--long", help[:long]) {
        |length| reset_length.do(site)
      }

      opts.on("--sites[=FILTER]", help[:sites]) {
        |filter_by| show_sites.do(filter_by); exit
      }

      opts.on("-h", "--help", help[:help]) {
        help opts; exit
      }

      opts.on("--new", help[:new]) {
        new_pass.for site; exit
      }

      opts.on("--show-key-words", help[:show_key_words]) {
        show_words.do; exit
      }

      opts.on("--set-key-from-key-words", help[:set_key_from_key_words]) {
        set_key.from ARGV; exit
      }

      opts.on("--remove", help[:remove]) {
        remove_site.do site; exit
      }

      opts.on("--new-master-key", help[:new_master_key]) {
        new_key.do; exit
      }
    end.parse!

    copy_pass.for site
  end
end
