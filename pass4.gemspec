require_relative 'lib/pass4/version'

Gem::Specification.new do |spec|
  spec.name          = "pass4"
  spec.version       = Pass4::VERSION
  spec.authors       = ["Row"]
  spec.email         = ["reardenr@gmail.com"]

  spec.summary       = %q{Password generator and manager}
  spec.description   = %q{Generates and retrieves distinct passwords for any site}
  spec.homepage      = "https://gitlab.com/RowDaBoat/pass4"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/RowDaBoat/pass4"
  spec.metadata["changelog_uri"] = "https://gitlab.com/RowDaBoat/pass4/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = "pass4"
  spec.require_paths = ["lib"]
end
